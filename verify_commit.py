#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import logging
import os
import re
import subprocess
import sys

import graypy
from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.serialization import pkcs7
from gitlab import Gitlab, GitlabGetError, GitlabError, GitlabAuthenticationError

from tool_version import __version__


def get_config_root():
    """
    Get config root: ``/opt`` for bundled build, script directory for unpacked script.

    :return: config root
    :rtype: str
    """
    if getattr(sys, 'frozen', False):
        return '/opt'

    return os.path.dirname(__file__)


GITLAB_INSTANCE = None
GITLAB_USERNAME = os.getenv('GL_USERNAME', 'dummy')
GITLAB_CONFIG_PATH = os.path.join(get_config_root(), '.gitlab.config')
LOG_FILE_PATH = os.path.join(get_config_root(), 'push_validation.log')
ZERO_COMMIT = '0000000000000000000000000000000000000000'
SIGN_CODE_MAPPING = {
    'U': 'unknown validity',
    'X': 'signature has expired',
    'Y': 'key has expired',
    'E': 'could not check signature (missing key?)',
    'R': 'key is revoked',
    'B': 'bad signature',
    'N': 'no signature provided'
}


class ValidationException(Exception):
    """
    Basic exception.
    """
    pass


class ProjectFilter(logging.Filter):
    """
    Logging filter allowing to add custom fields.

    .. seealso:: `logging.Filter <https://docs.python.org/3/library/logging.html#logging.Filter>`_
    """

    def __init__(self, name=''):
        super().__init__(name)
        self.project_id = os.getenv('GL_REPOSITORY', 'project-0').split('-')[1]

    def filter(self, record):
        """
        Add a custom property for logger (project ID).

        :param record: logger custom property
        :rtype: bool
        """
        if hasattr(record, 'graylog_only'):
            return False
        record.project_id = f'REPO-{self.project_id}'
        return True


class GraylogFilter(ProjectFilter):
    """
    Logging filter allowing to add graylog fields.

    .. seealso:: `logging.Filter <https://docs.python.org/3/library/logging.html#logging.Filter>`_
    """

    def __init__(self, name=''):  # noqa
        super().__init__(name)
        self.project_path = os.getenv('GL_PROJECT_PATH', 'ramazan-i/commit-signature-verifier')
        self.user = GITLAB_USERNAME
        self.commit_source = f'{os.getenv("GL_PROTOCOL", "ssh")}-{os.getenv("GL_ID", "key-1")}'

    def filter(self, record):
        """
        Add custom properties for graylog logger.

        :param record: logger custom property
        :rtype: bool
        """
        # Skip graylog while GITLAB_INSTANCE is unset and if skip_graylog == True
        if hasattr(record, 'skip_graylog') or not GITLAB_INSTANCE:
            return False
        record.gitlab = str(GITLAB_INSTANCE).replace('https://', '')
        record.project_id = self.project_id
        record.project_path = self.project_path
        record.user = self.user
        record.commit_source = self.commit_source
        record.level = record.levelname
        # Strip section breaks
        record.msg = str(record.msg).strip('=\n ')
        return True


def get_args(arguments=None):
    """
    Parse argparse arguments.

    .. seealso:: `argparse <https://docs.python.org/3/library/argparse.html>`_

    :param arguments: override arguments list
    :return: arguments namespace
    :rtype: :obj:`argparse.Namespace`
    """
    _parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description=f'Git Signature Verifier {__version__}',
        epilog='This tool verifies that certificate chain used to sign a commit is compliant with the provided Root CA.'
               f'\nAuth config File is located at {GITLAB_CONFIG_PATH} (make sure user has read permissions.'
               f'\nLog file is located at {LOG_FILE_PATH} (make sure user has write permissions).',
        usage=f'{{tool_path}} {{old commit SHA}} {{new commit SHA}} {{root CA path}}')

    _parser.add_argument('-o', '--old-sha', action='store', required=True, type=str.lower, help='Old object SHA')
    _parser.add_argument('-n', '--new-sha', action='store', required=True, type=str.lower, help='New object SHA')
    _parser.add_argument('-c', '--cert-path', action='store', required=True, type=str, help='Path to verified Root CA')
    _parser.add_argument('--graylog-server', action='store', type=str, help='Graylog server URL')
    _parser.add_argument('--graylog-port', action='store', type=int, help='Graylog server port')

    return _parser.parse_args(arguments)


def is_debug_flag_present(var_name):
    """
    Checks if variable is a valid debug flag.

    If ``GIT_PUSH_OPTION_? == debug.sign``, enable console debug logging.

    :param var_name: variable name
    :rtype: bool
    """
    return var_name.startswith('GIT_PUSH_OPTION_') and str(os.getenv(var_name)).lower() == 'debug.sign'


def get_configured_logger():
    """
    Get configured logging instance.

    :return: logging instance
    :rtype: :obj:`logging.Logger`
    """

    _base_formatter = logging.Formatter(f'%(message)s')
    _file_formatter = logging.Formatter(f'%(asctime)s %(levelname)-5s %(project_id)-10s %(message)s')
    _logger = logging.getLogger(__name__)
    _logger.setLevel(logging.DEBUG)

    # STDOUT handler
    ch = logging.StreamHandler()
    ch.setFormatter(_base_formatter)
    ch.addFilter(ProjectFilter())
    if any(map(lambda env_var: is_debug_flag_present(env_var), os.environ)):
        ch.setLevel(logging.DEBUG)
    else:
        ch.setLevel(logging.INFO)
    _logger.addHandler(ch)

    # File handler
    fh = logging.FileHandler(LOG_FILE_PATH)
    fh.setFormatter(_file_formatter)
    fh.addFilter(ProjectFilter())
    fh.setLevel(logging.DEBUG)
    _logger.addHandler(fh)

    # Graylog handler
    if args.graylog_server is not None and args.graylog_port is not None:
        gh = graypy.GELFHTTPHandler(args.graylog_server, args.graylog_port, debugging_fields=False, timeout=5)
        try:
            _tmp_logger = logging.getLogger('test-graylog')
            _tmp_logger.addHandler(gh)
            _tmp_logger.warning('test connection')
            gh.setFormatter(_base_formatter)
            gh.addFilter(GraylogFilter())
            gh.setLevel(logging.DEBUG)
            _logger.addHandler(gh)
        except:  # noqa
            _logger.debug('Graylog is disabled!')

    return _logger


def skip_branch_deletion():
    """
    Skip validation for branch deletion pushes.
    """
    if args.new_sha == ZERO_COMMIT:
        logger.debug(f'Skipped branch deletion push\n{"=" * 100}')
        sys.exit(0)


def read_root_cert_data(cert_path):
    """
    Load X509 certificate from file.

    :param cert_path: Root CA path
    :return: x509 certificate
    :rtype: :obj:`cryptography.x509.Certificate`
    """
    if not os.path.isfile(cert_path):
        print_on_error_and_exit('Could not read CA file data', show_usage=True)
    with open(cert_path, 'rb') as f:
        _root_ca = x509.load_pem_x509_certificate(f.read(), default_backend())

    return _root_ca


def print_on_error_and_exit(message, show_usage=False):
    """
    Print formatted error end exit with code 1.

    Message is formatted with specific GitLab hook prefix so that it's shown in Web UI.

    :param message: message to be printed
    :param show_usage: if ``True``, call :func:`get_args` with ``--help`` before exiting
    """
    logger.error(f'{message}\n{"=" * 100}')
    print(f'GL-HOOK-ERR: {message}. Please configure your git client as described in README', file=sys.stderr)
    if show_usage:
        get_args(['--help'])
    sys.exit(1)


def run_command(command, working_dir=os.getcwd()):
    """
    Execute command in subshell.

    :param command: string with instructions
    :param working_dir: working directory
    :return: process stdout
    :rtype: str
    """
    logger.debug(f'Executing "{command}"')
    process = subprocess.Popen(command,
                               shell=True,
                               cwd=working_dir,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT,
                               universal_newlines=True)
    stdout = process.stdout.read()
    process.wait()
    if process.returncode != 0:
        logger.fatal(f'Failed executing "{command}" (exit code: {process.returncode})\n\nOutput:\n{stdout}\n\n')
        sys.exit(1)
    return stdout


def is_gitlab_merge_commit(commit_sha):
    """
    Verify that merge commit is trusted.

    :param commit_sha: git object SHA
    :raises: :obj:`ValidationException`
    :rtype: bool
    """
    git_object = run_command(f'git cat-file -p {commit_sha}')
    # Merge commits always have 2 parents
    matches = re.search(r'parent (\w+)\nparent (\w+)', git_object)
    if matches:
        target_head_sha = matches.group(1).casefold()
        logger.debug(f'Matching target {target_head_sha}')

        source_head_sha = matches.group(2).casefold()
        logger.debug(f'Matching source {source_head_sha}')

        # Assuming no one changes the merge commit message
        _mr_match = re.search(r'See merge request.*!(\d+)', git_object)
        if not _mr_match:
            logger.debug('Found merge commit without a merge request: not a GitLab merge commit')
            return False
        merge_request_id = _mr_match.group(1)
        logger.debug(f'Found MR ID: {merge_request_id}')

        project = GITLAB.projects.get(GITLAB_PROJECT)
        try:
            mr = project.mergerequests.get(int(merge_request_id))
        except GitlabGetError:
            logger.debug(f'Assuming commit #{commit_sha[:8]} is not a real merge commit')
            # TODO: should we raise here?
            return False

        logger.debug(f'MR with source {mr.source_branch} and target {mr.target_branch}')
        remote_target_head_sha = project.branches.get(mr.target_branch).commit.get('id').casefold()
        remote_source_head_sha = project.branches.get(mr.source_branch).commit.get('id').casefold()

        # Just in case!
        if mr.attributes.get('draft'):
            raise ValidationException(f'!{merge_request_id} merge request is drafted and can not be merged')

        # Only good MRs are accepted
        if mr.attributes.get('merge_status') != 'can_be_merged':
            raise ValidationException(f'!{merge_request_id} merge request status: not ready')

        # Only matching target head MRs are accepted
        if target_head_sha != remote_target_head_sha:
            raise ValidationException(f'!{merge_request_id} merge request target HEAD SHA mismatch')

        # Squashed commits are just skipped
        if mr.attributes.get('squash'):
            logger.debug('skipping squashed commit check')
        elif source_head_sha != remote_source_head_sha:
            raise ValidationException(f'!{merge_request_id} merge request source HEAD SHA mismatch')

        return True

    return False


def validate_commit_signature(commit_sha):
    """
    Validate commit by reading signature status.

    Unsigned commits are checked to be valid merge request commits.

    .. seealso:: `Status codes <https://git-scm.com/docs/pretty-formats#Documentation/pretty-formats.txt-emGem>`_

    :param commit_sha: git object SHA
    :raises: :obj:`ValidationException`
    """
    logger.debug(f'Verifying commit #{commit_sha[:8]} signature validity')
    validation_status = run_command(f'git show {commit_sha} --pretty=format:"%G?" --quiet').upper()

    if validation_status == 'G':
        logger.debug(f'Successfully validated commit #{commit_sha[:8]} signature')
    elif validation_status == 'N':
        # GitLab merge commit is considered to be safe-unsigned
        logger.debug(f'Checking if commit #{commit_sha[:8]} is a merge commit')
        if is_gitlab_merge_commit(commit_sha):
            logger.debug(f'Verified merge commit #{commit_sha[:8]}, assuming further checks are not required')
            sys.exit(0)
        elif os.getenv('GL_PROTOCOL') == 'web':
            raise ValidationException(f'Web IDE pushing, patching and rebasing is unavailable (@{GITLAB_USERNAME}')
        else:
            raise ValidationException(f'{SIGN_CODE_MAPPING[validation_status]} (@{GITLAB_USERNAME})')
    else:
        raise ValidationException(f'{SIGN_CODE_MAPPING[validation_status]} (@{GITLAB_USERNAME})')


def get_signature_data(commit_sha):
    """
    Extract signed message in PEM format, convert headers to PKCS7 format.

    :param commit_sha: git object SHA
    :raises: :obj:`ValidationException`
    :return: PKCS7 data
    :rtype: str
    """
    logger.debug(f'Reading commit #{commit_sha[:8]} signature data')
    # Read signature type https://github.com/git/git/blob/644195e02f445bf4fcf79e47c339f593c7646c9e/gpg-interface.c#L35
    signature_type = re.match(r'.*-----BEGIN ([A-Z ]+)*-----', commit_info, re.DOTALL).group(1)
    if signature_type == 'SIGNED MESSAGE':
        logger.debug('Found x509 signature in commit data')
    # PGP SIGNATURE/PGP MESSAGE
    elif signature_type.startswith('PGP'):
        raise ValidationException('GPG signatures are not acceptable')
    elif signature_type == 'SSH SIGNATURE':
        raise ValidationException('SSH signatures are not acceptable')
    else:
        raise ValidationException('unknown type of signature found')
    # Using DOTALL to include newline + special chars
    pattern = re.compile(r'.*-----BEGIN SIGNED MESSAGE-----(?P<GPGSIG>.*)-----END SIGNED MESSAGE-----.*', re.DOTALL)

    match = pattern.match(commit_info)

    if match:
        logger.debug('Found signed message')
        gpgsig_lines = match.group('GPGSIG').split('\n')
        # gpgsig data is indented, stripping all spaces
        gpgsig_lines = list(filter(lambda x: x.lstrip(' '), gpgsig_lines))
        # Adding PKCS7 headers
        pkcs7_data = '-----BEGIN PKCS7-----\n'
        pkcs7_data += '\n'.join([x for x in gpgsig_lines])
        pkcs7_data += '\n-----END PKCS7-----'
    else:
        raise ValidationException('could not extract signature data, is commit signed?')

    return pkcs7_data


def get_extension_value(certificate_object, extension):
    """
    Read X509 extension from certificate.

    :param certificate_object: x509 certificate
    :param extension: type of extension to read
    :raises: :obj:`ValidationException`
    :return: extension value
    :rtype: str
    """
    subject_cn = certificate_object.subject.get_attributes_for_oid(x509.OID_COMMON_NAME)[0].value
    logger.debug(f'Reading {subject_cn} certificate {str(extension._name)}')  # noqa
    ext = certificate_object.extensions.get_extension_for_oid(extension)
    # Subject Key Identifier and Authority Key Identifier have different field names for the digest value
    try:
        if extension == x509.ExtensionOID.SUBJECT_ALTERNATIVE_NAME:
            value = ext.value.get_values_for_type(x509.RFC822Name)[0]
        else:
            value = getattr(ext.value, 'key_identifier')
    except Exception as e:
        raise ValidationException(f'caught an exception while getting extension value\n{e}')

    return value


def validate_certificate_chain(certs):
    """
    Validate full chain against Root CA by comparing SKIs/AKIs.

    :param certs: certificate chain
    :raises: :obj:`ValidationException`
    :rtype: bool
    """
    logger.debug(f'Verifying certificate chain against {args.cert_path} certificate authority')
    root_ca, intermediate_ca, owner_certificate = tuple(certs)

    # See conditions below
    issuer_ski_from_owner = get_extension_value(owner_certificate, x509.ExtensionOID.AUTHORITY_KEY_IDENTIFIER)
    real_issuer_ski = get_extension_value(intermediate_ca, x509.ExtensionOID.SUBJECT_KEY_IDENTIFIER)

    root_ski_from_intermediate = get_extension_value(intermediate_ca, x509.ExtensionOID.AUTHORITY_KEY_IDENTIFIER)
    real_root_ski = get_extension_value(root_ca, x509.ExtensionOID.SUBJECT_KEY_IDENTIFIER)

    # Owner certificate's authorityKeyIdentifier MUST match Intermediate CA subjectKeyIdentifier
    # Intermediate CA's authorityKeyIdentifier MUST match Root CA subjectKeyIdentifier
    # Root CA's subjectKeyIdentifier is derived from local file, not from chain!
    if (issuer_ski_from_owner == real_issuer_ski) and (root_ski_from_intermediate == real_root_ski):
        return True
    elif issuer_ski_from_owner != real_issuer_ski:
        raise ValidationException('target certificate issuer and intermediate certificate subject IDs mismatch')
    elif root_ski_from_intermediate != real_root_ski:
        raise ValidationException('intermediate certificate issuer and root certificate subject IDs mismatch')


def validate_commit_author(owner_cert, commit_sha):
    """
    Validate that committer email is a subject of owner certificate.

    :param owner_cert: owner certificate
    :param commit_sha: git object SHA to read committer email
    :raises: :obj:`ValidationException`
    :rtype: bool
    """
    logger.debug(f'Validating commit #{commit_sha[:8]} committer email')
    committer_email = run_command(f'git show {commit_sha} --format="format:%ce" --quiet')
    signer_email = get_extension_value(owner_cert, x509.ExtensionOID.SUBJECT_ALTERNATIVE_NAME)

    if committer_email.casefold() != signer_email.casefold():
        raise ValidationException(f'committer/signer email mismatch ({committer_email})/({signer_email})')


def check_if_commit_exists(commit_sha):
    """
    Check if object exists on remote.

    :param commit_sha: git object SHA
    :rtype: bool
    """
    project = GITLAB.projects.get(GITLAB_PROJECT)

    try:
        project.commits.get(commit_sha)
        return True
    except GitlabGetError:
        return False


def validate_commit(commit_sha):
    """
    Validate commit by performing several checks.

    :param commit_sha: git object SHA
    :raises: :obj:`ValidationException`
    """
    logger.debug(f'Verifying commit #{commit_sha[:8]} existence in repo')
    if check_if_commit_exists(commit_sha):
        logger.debug(f'Commit #{commit_sha[:8]} already exists, skipping further checks\n{"=" * 100}')
        return

    logger.debug(f'Commit #{commit_sha[:8]} does not exist in repo')

    # Validating signature status
    validate_commit_signature(commit_sha)
    # Reading raw PEM data from signed message
    signature_data = get_signature_data(commit_sha)
    # Loading extracted certificate chain
    certificates = pkcs7.load_pem_pkcs7_certificates(signature_data.encode())

    if len(certificates) != 2:
        # TODO: look carefully for this exact point! This might differ on different git/signing tool versions
        raise ValidationException('intermediate certificate not present in the signed message')
    # Adding root CA
    certificates.insert(0, root_cert_data)

    # Validating chain
    validate_certificate_chain(certificates)
    # Validating email
    validate_commit_author(certificates[-1], commit_sha)

    logger.debug(f'Successfully validated commit #{commit_sha[:8]} committer email\n{"=" * 100}')


def get_gitlab_instance():
    """
    Get GitLab instance by using private token.

    :return: GitLab instance
    :rtype: :obj:`gitlab.Gitlab`
    """
    try:
        with open(GITLAB_CONFIG_PATH) as cfg_file:
            global GITLAB_INSTANCE
            GITLAB_INSTANCE = cfg_file.readline().strip('\n')
            _gitlab_token = cfg_file.readline().strip('\n')
            _gl = Gitlab(GITLAB_INSTANCE, private_token=_gitlab_token)
            _gl.auth()
    except FileNotFoundError:
        print_on_error_and_exit('Could not find GitLab auth config!')
    except GitlabAuthenticationError:
        print_on_error_and_exit(f'Could not authenticate at GitLab instance {GITLAB_INSTANCE}')
    except GitlabError as gitlab_error:
        print_on_error_and_exit(f'Could not create a GitLab instance connection: {gitlab_error}')

    return _gl


def get_commit_objects():
    """
    Get commit object history based on full history or part of it.

    :return: list of commit SHAs
    :rtype: list[str]
    """
    logger.debug(f'Reading the object history', extra={'skip_graylog': True})
    if args.old_sha == ZERO_COMMIT:
        logger.debug('Branch creation push detected')
        _obj_list = run_command(f'git rev-list {args.new_sha} $(git for-each-ref --format "^%(refname:short)" '
                                f'refs/heads/)').split()
        logger.debug(f'Verifying {len(_obj_list)} commits')
        return _obj_list

    _obj_list = run_command(f'git log {args.old_sha}..{args.new_sha} --pretty="format:%H"').split()

    # TODO: check this case in details
    if not _obj_list:
        logger.debug(f'{"=" * 10} Reading reverse object history')
        _obj_list = run_command(f'git log {args.new_sha}..{args.old_sha} --pretty="format:%H"').split()
        logger.debug(f'Force push rollback detected; reverting {len(_obj_list)} commits')

    return _obj_list


if __name__ == '__main__':
    # Project path (i.e. ramazan-i/commit-signature-verifier)
    GITLAB_PROJECT = os.getenv('GL_PROJECT_PATH')

    args = get_args()

    logger = get_configured_logger()
    logger.debug(f'{"=" * 20} Git Signature Verifier {__version__} {"=" * 20}', extra={'skip_graylog': True})

    # Preparing gitlab instance (required for logging)
    GITLAB = get_gitlab_instance()

    # new_sha with ZERO_COMMIT value is to be skipped without validation
    skip_branch_deletion()

    # Reading trusted CA data
    root_cert_data = read_root_cert_data(args.cert_path)

    obj_list = get_commit_objects()
    if not obj_list:
        logger.debug('No new objects found')
    else:
        exception_list = []

        logger.debug(f'Processing history ({len(obj_list)} commit(s))')
        for git_obj in obj_list:
            try:
                # Dumping full commit data
                commit_info = run_command(f'git cat-file commit {git_obj}')
                logger.info(commit_info, extra={'graylog_only': True})
                validate_commit(git_obj)
            except ValidationException as validation_exception:
                exception_list.append(f'commit #{git_obj[:8]} ({str(validation_exception)})')
                logger.error(validation_exception, extra={'graylog_only': True})
                logger.error(f'Caught failure\n{"=" * 100}', extra={'skip_graylog': True})
            except Exception as unforeseen_exception:  # noqa
                logger.debug('Caught unforeseen exception!', extra={'skip_graylog': True})
                logger.debug(f'{unforeseen_exception.with_traceback(unforeseen_exception.__traceback__)}\n{"=" * 100}')

        if exception_list:
            msg = ';'.join(exception_list)
            print_on_error_and_exit(f'Caught "{GITLAB_PROJECT}" push validation errors: {msg}')
