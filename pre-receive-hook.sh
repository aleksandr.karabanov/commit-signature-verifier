#!/bin/bash
set -e

throw_hook_error() {
  local message="$@"
  echo "GL-HOOK-ERR: $@. Please contact DevOps Team immediately." >/dev/stderr
  exit 1
}

# Modify these paths!
VERIFY_TOOL_PATH=
if [[ ! -f "$VERIFY_TOOL_PATH" ]]; then
  throw_hook_error Could not find commit verification tool
fi

ROOT_CA_PATH=
if [[ ! -f "$ROOT_CA_PATH" ]]; then
  throw_hook_error Could not find root certificate authority
fi

# Allowlist repository groups: skip if verification has failed on specific groups
# i.e. if verification of allowlisted repo has failed, request the exitcode of $(true)
BLOCKER_CALLABLE=false
ALLOWLIST_PATH=
if [[ -f "$ALLOWLIST_PATH" ]]; then
  source "$ALLOWLIST_PATH"
fi

# Test
$VERIFY_TOOL_PATH --help >/dev/null || throw_hook_error Could not run commit verification tool

# Setting values from stdin
read ARGS </dev/stdin
OLD_OBJ_NAME="$(echo $ARGS | cut -d' ' -f1)"
NEW_OBJ_NAME="$(echo $ARGS | cut -d' ' -f2)"

# Debug specific cases
#if [[ "$GL_USERNAME" == "dummy_username" ]]; then
#    mkdir -p "/tmp/$GL_USERNAME"
#    cp -rf $GIT_DIR $GIT_EXEC_PATH $GIT_OBJECT_DIRECTORY "/tmp/$GL_USERNAME/"
#    env > "/tmp/$GL_USERNAME/env"
#    throw_hook_error Data dumped successfully
#fi

# Only verify head pushes
if [[ "${ARGS#*/}" =~ "heads/" ]]; then
  $VERIFY_TOOL_PATH -o $OLD_OBJ_NAME -n $NEW_OBJ_NAME -c $ROOT_CA_PATH || $BLOCKER_CALLABLE
fi
