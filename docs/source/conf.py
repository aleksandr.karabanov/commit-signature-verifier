# https://www.sphinx-doc.org/en/master/usage/configuration.html
import os
import sys

# -- Path setup --------------------------------------------------------------
sys.path.insert(0, os.getenv('CI_PROJECT_DIR', os.path.abspath('../..')))
from verify_commit import __version__ as version  # noqa

# -- Project information -----------------------------------------------------
project = 'Commit Signature Verifier'
copyright = '2022, Ramazan Ibragimov'
author = 'Ramazan Ibragimov'

# -- General configuration ---------------------------------------------------
extensions = [
    'sphinx.ext.autodoc',
    'sphinx_toolbox.collapse'
]

# templates_path = ['_templates']
# exclude_patterns = []

# -- Options for HTML output -------------------------------------------------
html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
html_css_files = ['style.css']
html_theme_options = {
    'display_version': True,
    'style_external_links': True,
    'collapse_navigation': False
}
pygments_style = 'monokai'

rst_prolog = f"""
.. |gitlab_url| replace:: {os.getenv('CI_PROJECT_URL')}/-/blob/{os.getenv('CI_COMMIT_REF_NAME')}/verify_commit.py
.. |tool| replace:: **Verifier**
"""

suppress_warnings = [
    'toc.circular'
]

# -- Localization options ----------------------------------------------------
language = 'en'
locale_dirs = ['_locales']
gettext_compact = False
trim_footnote_reference_space = True

# -- Extension options -------------------------------------------------------
autodoc_preserve_defaults = True
autodoc_default_options = {
    'member-order': 'bysource'
}
