:gitlab_url: |gitlab_url|

Commit Signature Verifier
#########################
    Git signature verification tool.

.. toctree::
    :hidden:

    index

Source code
***********

.. collapse:: verify_commit.py

    .. literalinclude:: ../../verify_commit.py
        :language: Python
        :linenos:

-----

Exception
*********

.. autoclass:: verify_commit.ValidationException


Generic helpers
***************

.. autofunction:: verify_commit.get_config_root
.. autofunction:: verify_commit.is_debug_flag_present
.. autofunction:: verify_commit.print_on_error_and_exit
.. autofunction:: verify_commit.run_command

Logging
*******

Logging has 3 handlers:

1. :obj:`StreamHandler`
    Streams log to ``stdout``. Default level: ``WARNING``.
2. :obj:`FileHandler`
    Streams log to ``{config_root}/push_validation.log``. Default level: ``DEBUG``.
3. :obj:`GraylogHandler`
    Streams log to Graylog service. Default level: ``DEBUG``.

:obj:`StreamHandler` log level is defined by :func:`verify_commit.is_debug_flag_present`.

.. seealso::
    * `logging.Handlers <https://docs.python.org/3/library/logging.html#handler-objects>`_
    * `graypy Usage <https://graypy.readthedocs.io/en/latest/readme.html#usage>`_

Tool workflow
*************

Contents of ``if __name__ == '__main__'``.

Preparation
===========

1. |tool| reads GitLab project path from the environment variable. `Reference <https://docs.gitlab.com/ee/administration/server_hooks.html#environment-variables-available-to-server-hooks>`_.
2. |tool| gets launch arguments.

.. autofunction:: verify_commit.get_args

3. |tool| logger is configured.

.. rubric:: Logging facility

..


    .. autofunction:: verify_commit.get_configured_logger

    .. autoclass:: verify_commit.ProjectFilter
        :members:

    .. autoclass:: verify_commit.GraylogFilter
        :members:

4. |tool| logs initial message.
5. |tool| prepares GitLab session instance.

.. autofunction:: verify_commit.get_gitlab_instance

6. |tool| checks if the push deletes a branch.

.. autofunction:: verify_commit.skip_branch_deletion

7. |tool| reads Root CA data.

.. autofunction:: verify_commit.read_root_cert_data

8. |tool| gets commit diff from history.

.. autofunction:: verify_commit.get_commit_objects

    * If **there are no new objects**, exit.
    * In other case, process the history.

Processing object history
=========================

.. note:: Every raise of :obj:`verify_commit.ValidationException` breaks the sequence.

    Exception information in this case is added to a list.

1. |tool| iterates over object list.

|tool| reads commit info with :func:`verify_commit.run_command`: ``git cat-file commit {commit_sha}``.

The number of checks are performed with the following function:

.. autofunction:: verify_commit.validate_commit

    1. |tool| calls :func:`check_if_commit_exists`.

    .. autofunction:: verify_commit.check_if_commit_exists

    * If **commit exists**, skip further checks.
    * In other case, proceed.

    2. |tool| calls :func:`validate_commit_signature`.

    .. autofunction:: verify_commit.validate_commit_signature

        1. |tool| validates signature status.

        |tool| reads signature status with :func:`verify_commit.run_command`: ``git show {commit_sha} --pretty=format:"%G?" --quiet``.

        * If status is **good**, signature check is passed.
        * If status is **not good**, raise :obj:`ValidationException`.
        * If commit is **not signed**, run more checks.

        2. |tool| calls :func:`is_gitlab_merge_commit`.

        .. autofunction:: verify_commit.is_gitlab_merge_commit

        * If this is a **valid GitLab merge commit**, signature check is passed.
        * In other case, run more checks.

        3. |tool| checks if unsigned commit is a web-pushed commit.

        4. |tool| raises :obj:`ValidationException`.

    3. |tool| calls :func:`get_signature_data`.

    .. autofunction:: verify_commit.get_signature_data

    4. |tool| loads certificate chain.
    5. |tool| calls :func:`validate_certificate_chain`.

    .. autofunction:: verify_commit.validate_certificate_chain

    6. |tool| calls :func:`validate_commit_author`.

    .. autofunction:: verify_commit.validate_commit_author

    7. |tool| finishes check.

2. |tool| checks whether there are exceptions caught during checking.

    * If there are no exceptions, exit.
    * In other case, process the exceptions.

Processing exception list
=========================

1. Exceptions are joined with ``;``.
2. |tool| calls :func:`verify_commit.print_on_error_and_exit` with merged exceptions message.